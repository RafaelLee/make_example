My Awesome Makefile
===================

This is a project of automatically detects source file and compiles. It shows the power of GNU make.
If you want to test some C/C++ code fast, use the Makefile in this project will save a lot of time.

Basically, it detects all source code in the project automatically, and then compile the source into an executable.

Advantages:
Detect the dependencies automatically, compile downstream files when upstream header files updated.

Limitations:
All source codes are compiled with the same set of parameters.
External dependencies need to add to this project manually.
If not all source in the sub folder need to be compiled, specify the source files manually in those variables:

```Makefile
all_c_files :=
all_cc_files :=
all_asm_files :=
```
