#include <stdio.h>
#include <stdint.h>

#include "main.hh"

#ifdef __cplusplus
extern "C" {
  extern int my_add (int a, int b);
}
#endif /* __cplusplus */

int main (void)
{
  uint32_t i32 = 0x12;
  uint8_t i8 = 0x12;


  printf ("%d\n", my_add (3, 4));
  // assert (("Big Endian, not write" , (uint32_t *)(&i8) == i32));

  return 0;
}


